tdd-docker-dev-template
=======================

This is a template repository to get you started doing test-driven docker development. It relies on the excellent [serverspec](http://serverspec.org/) infastructure testing framework, giving you access to resources such as `command`:

```ruby
describe command('ls /foo') do
  its(:stderr) { should match /No such file or directory/ }
end
```

These tests execute against a docker image defined by your `Dockerfile`. Currently, there's no way to guarantee that the image will be removed after the test (since the test execution might still be running the container it put it on) so after you are done you might want to remove the images created by your tests. They'll be easy to spot because they wont have a repo or tag. Just `docker rmi <image hash>` and you are gtg. If your know how to delay the image remove call in an after -- awesome. PR and fix it.


Dependencies
------------

1. [Ruby >= 1.9.3](https://www.ruby-lang.org/en/).

2. If on non-windows: [ruby-install](https://github.com/postmodern/ruby-install) and [chruby](https://github.com/postmodern/chruby) (keeps your work out of your system ruby -- optional).

3. [Bundler](http://bundler.io/).

Getting Started
---------------

1. Clone this repository.

1. Open a terminal.

1. <Optional> `ruby-install ruby-1.9.3`

1. <Optional> `chruby_used <path-from-above>`

1. `bundle install`

1. `bundle exec guard`

The spec file
-------------

All tests go in `spec/` and have a name ending in `_spec.rb`. The `docker/whalesay fortune` demo is conveniently demonstrated for you in `spec/fortunes_spec.rb` and the repo `Dockerfile`
