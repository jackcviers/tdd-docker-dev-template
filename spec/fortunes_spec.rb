require 'rubygems'
require 'bundler/setup'
require 'serverspec'
require 'docker'

describe "fortune" do
  before(:all) do
    @image = Docker::Image.build_from_dir('.')
    set :os, family: :debian
    set :backend, :docker
    set :docker_image, @image.id
  end



  describe package('fortunes') do
    it { should be_installed }
  end

  describe command('ls -al /usr/games') do
    its(:stdout) { should contain("fortune") }
  end

  describe command('/usr/games/fortune') do
    its(:exit_status) { should eq 0}
  end
end
